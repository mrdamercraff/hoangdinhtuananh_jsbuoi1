/**
 * tính tiền lương nhân viên
 * 
 * input:
 * hằng số:Lương :100.000 
 * biến số : số ngày làm gọi là a
 * 
 * 
 * step
 * gọi hằng số Lương 1ngày = 100.000
 * gọi biến số ngày làm = 10
 * gọi biến số lương = b
 * lương bằng ngày làm * lương 1 ngày
 * 
 * out put
 * gọi biến số lương
 */

const oneDaySalary = 100000;
var workDay = 15;
var Salary;
Salary = oneDaySalary * workDay
console.log("Salary =", Salary  );

/**
 * BAI 2 TINH GIA TRI TRUNG BINH
 * INPUT 
 * GOI 5 SO NGAU NHIEN
 * 
 * STEP
 * CONG TONG 5 SO NGAU NHIEN CHIA CHO 5 = KET QUA
 * 
 * OUTPUT
 * 
 * HIEN THI KET QUA
 * 
 */

var number1 = 10;
var number2 = 20;
var number3 = 30;
var number4 = 40;
var number5 = 50;
var mean5Numbers;
mean5Numbers = (number1 + number2 + number3 + number4 + number5) / 5;
console.log("Result bai 2 =", mean5Numbers);

/**
 * BAI 3 QUY DOI TIEN
 * INPUT
 * GOI HANG SO USD = 23500
 * GOI BIEN SO SO TIEN = a
 * 
 * STEP
 * VND = USD * a
 * 
 * OUTPUT
 * 
 * HIEN THI VND
 */

const USD = 23500
var numberOfBills = 4
var VND;
VND = USD * numberOfBills;
console.log("Result bai 3 =", VND);

/**
 * Bai 4: Tinh dien tich, chu vi hinh chu nhat
 * 
 * INPUT
 * Chieu dai = a
 * chieu rong = b
 * a>b
 * 
 * Step
 * dien tich = a*b
 * chu vi = (a+b)*2
 * 
 * output
 * 
 * ket qua:
 * dien tich
 * chuvi
 */

var length = 6;
var width = 4;
var area;
var perimeter;
area = length * width;
perimeter = (length + width) / 2;
console.log("result bai 4 area =", area);
console.log("result bai 4 perimeter =", perimeter);

/**
 * Bai 5: tinh tong 2 ky so
 * Input
 * so co 2 ky so = a
 * 
 * Step
 * hang don vi = a%10; (chia lay du)
 * hang chuc = a / 10 (chia lam tron)
 * ket qua = hang don vi + hang chuc
 * 
 * Output
 * ket qua
 */

var number = 83;
var units;
var tenOf;
var Result5;
units = number % 10;
tenOf =Math.floor( number / 10);
Result5 = units + tenOf;
console.log("result bai 5 =", Result5);


